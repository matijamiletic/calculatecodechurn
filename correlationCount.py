# -*- coding: utf-8 -*-

#U cetvrtoj fazi program racuna korelaciju izmedu svih metrika i njihovih
#razlika i broja neispravnosti.

#uvozi knjiznica csv za upravljanje csv datotekama
import csv
#uvozi se knjiznica pomocu koje cemo racunati faktor korelacije
import scipy
import scipy.stats
#uvozi se knjiznica pomocu koje ce se iscrtavati grafovi za vizualni prikaz korelacije
import matplotlib.pyplot as plt

#otvaramo datoteku u kojoj se nalaze rezultati usporedbe trenutne verzije i najstarijeg pojavljivanja svake datoteke unutar te verzije
readerCurrentFile = open("/home/monika/Documents/upi/writeOldestResults.csv", 'r')
readerCurrentFileDict = csv.DictReader(readerCurrentFile, delimiter=",", quoting=csv.QUOTE_NONE)

#stvaramo liste koja ce sadrzavati apsolutne i relativne razlike svake metrike, bug count te sve metrike
absoluteDifferenceArray = []
relativeDifferenceArray = []
bugCntArray = []
metrics = []

#postavljamo flag na true koji ce provjeravati ukoliko je korisnik unio postojecu metriku
flag = 1
#trazimo od korisnika unos metrike dok god ne unese postojecu metriku
while flag:
	#trazimo od korisnika unos metrike
    metric = raw_input("Please enter metric for correlation count: ")

	#ako je korisnik unio File ili bug_cnt to nisu metrike, postavljamo flag na True te ga zatrazujemo da ponovno upise metriku
    if metric == "File" or metric == "bug_cnt":
        flag = 1
        print "Metric does not exist. Please enter existing metric."
    else:
		#u suprotnom postavljamo korisnikov unos na velika slova jer su metrike zapisane velikim slovima kao kratice
        metric = metric.upper()
		#provjerava se ukoliko metrika postoji
        if metric in readerCurrentFileDict.fieldnames:
			#ako metrika postoji postavlja se flag na False
            flag = 0
        else:
			#ako metrika ne postoji postavlja se flag na True te od korisnika ponovno trazimo unos
            flag = 1
            print "Metric does not exist. Please enter existing metric."

#prolazimo kroz rjecnik koji sadrzi sve retke datoteke writeOldestResult
for index, row in enumerate(readerCurrentFileDict):
	#spremamo bug count iz svakog retka
	bugCntArray.append(float(row['bug_cnt']))
	
	#spremamo metriku iz svakog retka
	metrics.append(row[metric])
	
	#spremamo apsolutnu razliku iz svakog retka za odabranu metriku
	absoluteDifferenceArray.append(row['Absolute difference ' + metric])
	
	#spremamo relativnu razliku iz svakog retka za odabranu metriku
	relativeDifferenceArray.append(row['Relative difference ' + metric])

#ispisuje se metrika koju je korisnik unio, sukladno sa svim bug countovima, metrikama te apsolutnim i relativnim razlikama
print "Choosen metric: ", metric
print "BugCnt: ", bugCntArray
print "Metrics: ", metrics
print "Absolute metric: ", absoluteDifferenceArray
print "Relative metric: ", relativeDifferenceArray

#pripremamo figuru za iscrtati graf, te ax1 i ax2 varijable koje ce sadrzavati svaka svoj scatter dijagram
#postavljamo zajednicku y os
f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)

#postavlja se naslov scatter diagrama za prikaz korelacije relativne razlike i bug count-a
ax1.set_title("Relative difference and bug count correlation")
#izradjuje se scatter diagram, postavlja crvena boja tocaka, marker o koji ce nam prikazati tockasti dijagram, te label na Relative
ax1.scatter(relativeDifferenceArray, bugCntArray, color="r", marker="o", label="Relative")
#postavljuje se ime y osi, ona je zajednicka
ax1.set_ylabel("Bug count")
ax1.set_ylim([0, max(bugCntArray)+10])
#postavlja se ime x osi za prvi dijagram
ax1.set_xlabel("Relative difference " + metric)
#izracunava se Spearmanov faktor korelacije izmedju relativnih razlika i bug count-a
corrR, pvalR = scipy.stats.spearmanr(relativeDifferenceArray, bugCntArray)
#postavljamo anotaciju koja ce ispisati faktor korelacije
ax1.annotate("Correlation coefficient: " + str(corrR), (0,0), (0, max(bugCntArray)+5))

#postavlja se naslov scatter diagrama za prikaz korelacije apsolutne razlike i bug count-a
ax2.set_title("Absolute difference and bug count correlation")
#izradjuje se scatter diagram, postavlja plava boja tocaka, marker o koji ce nam prikazati tockasti dijagram, te label na Absoulte
ax2.scatter(absoluteDifferenceArray, bugCntArray, color="b", marker="o", label="Absoulte")
#postavlja se ime x osi za drugi dijagram
ax2.set_xlabel("Absolute difference " + metric)
#izracunava se Spearmanov faktor korelacije izmedju apsolutnih razlika i bug count-a
corrA, pvalA = scipy.stats.spearmanr(absoluteDifferenceArray, bugCntArray)
#postavljamo anotaciju koja ce ispisati faktor korelacije
ax2.annotate("Correlation coefficient: " + str(corrA), (0,0), (0, max(bugCntArray)+5))

#prikazuju se dijagrami
f.show()