import os
import sys
import csv
import glob
import re
import pandas
import os.path

#unos putanje i trenutne verzije, prazan input se ne prihvaca
flag = 1;
while flag:
	path = raw_input("Please enter path: ")

	if not path:
		print "Please enter path again!"
	else:
		#dohvat putanje direktorija koji sadrzi sve verzije programskog koda
		fmask = os.path.join(path, '*.csv')
		csvFiles = glob.glob(fmask)
		if not csvFiles:
			print "Please enter path again. There is no files under that path."
		else:
			flag = 0

#ispis svih csv datoteka uz indekse

print "Available versions:"
i = 0
files = []
for index, file in sorted(enumerate(csvFiles), key=lambda file: file[1]):
	print str(i) + " - " + file #dohvati sve csv fileove iz zadanog direktorija
	files.append(file)
	i=i+1

#od korisnika se trazi unos indeksa trenutne verzije
flag = 1;
while flag:
	currentFileIndex = raw_input("Please enter current version index: ")
	#provjerava se je li indeks trenutne verzije datoteke validan
	if currentFileIndex.isdigit() and int(currentFileIndex) < len(csvFiles):
		#postavlja se indeks trenutne verzije i sprema se trenutna verzija datoteke u currentFile
		currentFileIndex = int(currentFileIndex)
		currentFile = files[currentFileIndex]
		flag = 0;
	else:
		print "Current version index is not valid."

#otvaranje trenutne verzije datoteke te zapisivanje rijecnika u readerCurrentFileDict varijablu
readerCurrentFile = open(currentFile, 'r')
readerCurrentFileDict = csv.DictReader(readerCurrentFile, delimiter=",", quoting=csv.QUOTE_NONE)

#izrada polja headers i ucitavanje imena klase te svih metrika trenutne verzije
headers = []
headersMetric = readerCurrentFileDict.fieldnames
headersLenMetric = len(headersMetric)

print "Comparing started. Please wait."
case1 = 0
case2 = 0
case3 = 0
case4 = 0
bug_cnt_bin = -1
difference = False
classFound = -1
newBug = 0
case2new = 0

writeFileName = re.split(r'(\W+)', currentFile)
writeFileName =  writeFileName[2] + "_withoutCase12.csv"
writer = open(writeFileName, 'w')
writer = csv.DictWriter(writer, delimiter=",", fieldnames=headersMetric, lineterminator='\n')
writer.writeheader()

writeFileName1 = re.split(r'(\W+)', currentFile)
writeFileName1 =  writeFileName1[2] + "_withoutCase1.csv"
writer1 = open(writeFileName1, 'w')
writer1 = csv.DictWriter(writer1, delimiter=",", fieldnames=headersMetric, lineterminator='\n')
writer1.writeheader()

writeFileNameTablica = re.split(r'(\W+)', currentFile)
naslov = writeFileNameTablica[2]    
writeFileNameTablica =  writeFileNameTablica[2] + "_tablica.csv"
completeName = os.path.join(".\TABLICA\\", writeFileNameTablica)  
writerTablica = open(completeName, 'w')
headersTablica = [naslov,"Tip","Ukupno", "New", "Old"]
writerTablica = csv.DictWriter(writerTablica, delimiter=",", fieldnames=headersTablica, lineterminator='\n')
writerTablica.writeheader()

for indexCurrentVersion, currentVersionRow in enumerate(readerCurrentFileDict):
	bug_cnt_bin = currentVersionRow["bug_cnt_bin"]
	difference = False
	for i in range(1, headersLenMetric):
		#print currentVersionRow[headersMetric[i]]
		if "ClassFound" in headersMetric[i]:
			classFound = currentVersionRow[headersMetric[i]]
		 
		if "Absolute difference" in headersMetric[i] or "Relative difference" in headersMetric[i]:
			if float( currentVersionRow[headersMetric[i]] ) > 0.0 and "bug_cnt" not in headersMetric[i]:
				#print headersMetric[i]
				difference = True
				break
				
	if bug_cnt_bin == "0" and difference == False:
		case1+=1
		if classFound == "0":
			case2new+=1
	elif bug_cnt_bin == "1" and difference == False:
		case2+=1
		if classFound == "0":
			newBug+=1
		writer1.writerow(currentVersionRow)
	elif bug_cnt_bin == "0" and difference == True:
		case3+=1
		writer.writerow(currentVersionRow)
		writer1.writerow(currentVersionRow)
	elif bug_cnt_bin == "1" and difference == True:
		case4+=1
		writer.writerow(currentVersionRow)
		writer1.writerow(currentVersionRow)

prviRedak = {}
prviRedak["Tip"] = "Nema promjene u verziji, bug cnt = 0"
prviRedak["Ukupno"] = case1
prviRedak["New"] = case2new	
prviRedak["Old"] = case1-case2new
writerTablica.writerow(prviRedak)

drugiRedak = {}
drugiRedak["Tip"] = "Nema promjene u verziji, bug cnt = 1"
drugiRedak["Ukupno"] = case2
drugiRedak["New"] = newBug	
drugiRedak["Old"] = case2-newBug
writerTablica.writerow(drugiRedak)

treciRedak = {}
treciRedak["Tip"] = "Postoje promjene u verziji, bug cnt = 0"
treciRedak["Ukupno"] = case3
treciRedak["New"] = 0	
treciRedak["Old"] = 0
writerTablica.writerow(treciRedak)

cetvrtiRedak = {}
cetvrtiRedak["Tip"] = "Postoje promjene u verziji, bug cnt = 1"
cetvrtiRedak["Ukupno"] = case4
cetvrtiRedak["New"] = 0	
cetvrtiRedak["Old"] = 0
writerTablica.writerow(cetvrtiRedak)
	
print "Nema promjene u verziji, bug cnt = 0: " + str(case1) + ", tek nastale: " + str(case2new)
print "Nema promjene u verziji, bug cnt = 1: " + str(case2) + ", tek nastale: " + str(newBug)
print "Postoje promjene u verziji, bug cnt = 0: " + str(case3)
print "Postoje promjene u verziji, bug cnt = 1: " + str(case4)
	
"""
#stvaranje varijable koja ce sadrzavati sve pronadjene klase unutar trenutne verzije u nekoj od starijih verzija
foundClasses = []
#iteracija kroz retke trenutne verzije
for indexCurrentVersion, currentVersionRow in enumerate(readerCurrentFileDict):	
	#iteracija kroz sve verzije koje su starije od trenutne verzije
	for indexCurrentCompareVersion, currentCompareFile in enumerate(readerCompareList):
		#postavljanje trenutne usporedne datoteke na pocetak
		readerCompareListFiles[indexCurrentCompareVersion].seek(0)
		#za svaku iteraciju osim prve pomakni na drugi redak jer prvi sadrzi header
		if indexCurrentVersion != 0:
			readerCompareListFiles[indexCurrentCompareVersion].next()

		#postavljanje flag varijable na 0, varijabla prati da li klasa postoji u nekoj od starijih verzija
		flag = 0
		for compareVersionRow in currentCompareFile:
			#ako je klasa vec pronadjena u nekoj od prethodnih verzija postavlja se flag na true i preskace se izvodjenje petlje za tu klasu
			if currentVersionRow['File'] in foundClasses:
				flag = 1
				continue
			if currentVersionRow['File'] == compareVersionRow['File']: #ako klasa postoji u obje datoteke
				#racunanje defect density
				currentVersionRow['DD'] = float(currentVersionRow['bug_cnt']) / float(currentVersionRow['LOC'])
				#racunanje prosjecnog broja gresaka
				currentVersionRow['bug_cnt_avg'] = float(currentVersionRow['bug_cnt']) / float(bug_cnt_SUM)
				#racunanje prosjecnog broja linija koda (LOC)
				currentVersionRow['LOC_avg'] = float(currentVersionRow['LOC']) / float(LOCsum)
				#postavljanje vrijednosti klasifikatora na 0 ili 1 ovisno o broju LOC-a
				if (float(currentVersionRow['bug_cnt']) > 0.0 ):
					currentVersionRow['bug_cnt_bin'] = 1
				else:
					currentVersionRow['bug_cnt_bin'] = 0
				#postavljanje flaga na 1 i dodavanje true vrijednosti u stupac ClassFound
				flag = 1
				currentVersionRow['ClassFound'] = '1'
				currentVersionRow['Version'] = csvFiles[indexCurrentCompareVersion]
				#iteracija kroz koju se racunaju apsolutna i relativna razlika
				#nakon izracuna dobivene vrijednosti zapisuju se u odgovarajuce stupce
				for i in range(1, headersLenMetric):
					#izracun apsolutne razlike
					absoluteDifference = abs(float(currentVersionRow[headersMetric[i]]) - float(compareVersionRow[headersMetric[i]]))
					#zapisivanje apsultne razlike
					currentVersionRow['Absolute difference ' + headersMetric[i]] = str(absoluteDifference)
					#formatiranje relativne razlike na float vrijednost s dvije decimale ako je metrika 0
					if float(currentVersionRow[headersMetric[i]]) == 0.0:
						relativeDifference = format(0, '.2f')
					#racunanje relativne razlike te formatiranje relativne razlike na dvije decimale
					else:
						relativeDifference = format(( absoluteDifference / float(currentVersionRow[headersMetric[i]]) ), '.2f')#.replace('.', ',')
					#zapisivanje relativne razlike
					currentVersionRow['Relative difference ' + headersMetric[i]] = str(relativeDifference)
				#dodavanje klase u listu pronadjenih klasa
				foundClasses.append(currentVersionRow['File'])
		#ako nismo pronasli klasu u niti jednoj prethodnoj verziji
		if flag == 0:
			#racunanje defect density
			currentVersionRow['DD'] = float(currentVersionRow['bug_cnt']) / float(currentVersionRow['LOC'])
			#racunanje prosjecnog broja gresaka
			currentVersionRow['bug_cnt_avg'] = float(currentVersionRow['bug_cnt']) / float(bug_cnt_SUM)
			#racunanje prosjecnog broja linija koda (LOC)
			currentVersionRow['LOC_avg'] = float(currentVersionRow['LOC']) / float(LOCsum)
			#postavljanje vrijednosti klasifikatora na 0 ili 1 ovisno o broju LOC-a
			if (float(currentVersionRow['bug_cnt']) > 0.0 ):
				currentVersionRow['bug_cnt_bin'] = 1
			else:
				currentVersionRow['bug_cnt_bin'] = 0
			#postavljanje ClassFound na false
			currentVersionRow['ClassFound'] ='0'
			#Version se postavlja na none jer klasa nije pronadjena u niti jednoj prethodnoj verziji
			currentVersionRow['Version'] = 'none'
			#iteracija kroz koju se zapisuju false vrijednosti za sve metrike jer nismo pronasli navedenu klasu
			for i in range(1, headersLenMetric):
				currentVersionRow['Absolute difference ' + headersMetric[i]] = '0'
				currentVersionRow['Relative difference ' + headersMetric[i]] = '0'
	#zapisujemo redak u izlaznu datoteku
	writer.writerow(currentVersionRow)
"""
#korisnika se obavjestava o uspjesnoj provedbi programa
print "Comparing info: success"

